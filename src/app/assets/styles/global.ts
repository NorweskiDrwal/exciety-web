import { createGlobalStyle } from 'styled-components';

export const GlobalStyles = createGlobalStyle`
    body {
        margin: 0;
        padding: 0;
        width: 100vw;
        height: 100vh;
        line-height: 1.5;
        font-size: 16px;
        position: relative;
        box-sizing: border-box;
        background: #f5f5f5;
        font-family: 'Josefin Sans', sans-serif;
    }

    a {
        text-decoration: none;
    }
    
    * {
        overflow: hidden;
    }
`;
