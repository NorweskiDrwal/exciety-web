import { css } from 'styled-components';
import { Spacing } from '..';

const Centered = css`
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    margin: auto;
    position: absolute;
`;

const Input = css`
    width: calc(100% - 24px);
    border: none;
    font-size: 1rem;
    border-radius: 0.5rem;
    padding: ${Spacing.ML};
    background-clip: padding-box;
    font-family: 'Josefin Sans', sans-serif;
    :focus {
        outline: none;
    };
`;

export {
    Centered,
    Input,
};
