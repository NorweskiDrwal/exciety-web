export const Spacing = {
    S: '4px',
    SM: '6px',
    M: '8px',
    ML: '12px',
    L: '16px',
    LXL: '20px',
    XL: '24px',
    XXL: '32px',
    XXXL: '50px',
};
