import { AnyAction } from 'redux';

interface Action<P> extends AnyAction {
    readonly type: string;
    readonly payload: Readonly<P>;
}

export class ActionDefinition<P> {
    private constructor (
        private readonly type: string,
    ) {}
    
    static of<P = void>(type: string) {
        return new ActionDefinition<P>(type);
    }
    
    create = (payload: P): Action<P> =>
        ({ payload, type: this.type });
    
    is (action: AnyAction): action is Action<P> {
        return action.type === this.type;
    }
}
