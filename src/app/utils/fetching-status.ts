export enum FetchingStatus {
    PENDING = 'PENDING',
    LOADING = 'LOADING',
    SUCCESS = 'SUCCESS',
    FAILURE = 'FAILURE',
}
