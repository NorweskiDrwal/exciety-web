import * as React from 'react';

const NoMatch: React.FunctionComponent = (p) => (
    <p>404 - try different url</p>
);

export default NoMatch;
