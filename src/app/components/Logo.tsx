import * as React from 'react';
import styled, { keyframes } from 'styled-components';

import logo from 'src/app/assets/images/logo.png';

const pop = keyframes`
    0% { opacity: 0 }
    100% { opacity: 1 }
`;

const ExcietyLogo = styled.img`
    top: 10px;
    left: 50%;
    opacity: 1;
    height: 50px;
    position: absolute;
    transform: translateX(-50%);
    animation: ${pop} 0.15s linear;
`;

const Logo: React.FunctionComponent = () => (
    <ExcietyLogo src={logo} alt="Exciety logo"/>
);

export default Logo;
