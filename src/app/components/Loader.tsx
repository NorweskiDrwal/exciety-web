import * as React from 'react';
import styled, { keyframes } from 'styled-components';
import { Colors } from 'src/app/assets';

import icon from 'src/app/assets/images/icon.png';

const Ring = styled('div')<{ small: boolean }>`
    position: absolute;
    border-radius: 50%;
    box-sizing: border-box;
    border: 10px solid transparent;
    width: ${props => props.small ? 'none' : 'inherit'};
    height: ${props => props.small ? 'none' : 'inherit'};
`;

const spin = keyframes`
    0% { transform: rotate(0deg) }
    100% { transform: rotate(360deg) }
`;

const pop = keyframes`
    0% { height: 0 }
    60% { height: 150px }
    100% { height: 130px }
`;

const Styled = {
    Wrapper: styled('div')<{ small: boolean }>`
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        margin: auto;
        z-index: 10000;
        position: absolute;
        width: ${props => props.small ? 'none' : '150px'}
        height: ${props => props.small ? 'none' : '150px'}
    `,
    ExcietyIcon: styled.img`
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        margin: auto;
        height: 130px;
        position: absolute;
        animation: ${pop} 0.4s ease-out;
    `,
    Central: styled(Ring)` && {
        border-color: ${Colors.yellow};
    }`,
    Top: styled(Ring)` && {
        border-top-color: ${Colors.white};
        animation: ${spin} 0.5s linear infinite;
    }`,
    Right: styled(Ring)` && {
        border-right-color: ${Colors.blue};
        animation: ${spin} 2s linear infinite;
    }`,
    Bottom: styled(Ring)` && {
        border-bottom-color: ${Colors.pink};
        animation: ${spin} 1s linear infinite;
    }`,
    Left: styled(Ring)` && {
        border-left-color: ${Colors.red};
        animation: ${spin} 1.5s linear infinite;
    }`,
};

type Props = {
    small?: boolean,
}

const Loader: React.FunctionComponent<Props> = (props) => (
    <Styled.Wrapper small={!!props.small}>
        <Styled.Central small={!!props.small}/>
        <Styled.Top small={!!props.small}/>
        <Styled.Right small={!!props.small}/>
        <Styled.Bottom small={!!props.small}/>
        <Styled.Left small={!!props.small}/>
        <Styled.ExcietyIcon src={icon} alt="Exciety"/>
    </Styled.Wrapper>
);

export default Loader;
