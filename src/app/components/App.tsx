import * as React from 'react';
import { connect } from 'react-redux';
import { ConnectedRouter as RouterProvider } from 'connected-react-router';

import { history } from 'src/app/store';
import { GlobalStyles } from 'src/app/assets';
import { LayoutContainer } from 'src/app/components';
import { ReduxProps } from 'src/app/utils';

import * as AccessAsyncActions from 'src/access/async-actions';

const mapDispatch = {
    fetchUser: AccessAsyncActions.fetchUser,
};

type Props = ReduxProps<any, typeof mapDispatch>;

class App extends React.Component<Props> {
    componentDidMount() {
        this.props.fetchUser();
    }
    
    render() {
        return (
            <>
                <RouterProvider history={history}>
                    <LayoutContainer/>
                </RouterProvider>
                <GlobalStyles/>
            </>
        )
    }
}

export default connect(null, mapDispatch)(App);
