import * as React from 'react';
import { connect } from 'react-redux';
import { Route, RouteComponentProps, Switch, withRouter } from 'react-router';

import { RootState } from 'src/app/store';
import { Loader, Logo, NoMatch } from 'src/app/components';
import { FetchingStatus, ReduxProps } from 'src/app/utils';

import { SetupContainer } from 'src/setup/components';
import { AccessContainer } from 'src/access/components';
import { ProfileContainer } from 'src/profile/components';

const mapState = (state: RootState) => ({
    isAuthenticated: state.app.isAuthenticated,
    uid: state.access.user && state.access.user.uid,
    isLoading: state.access.status && state.access.status === FetchingStatus.LOADING,
});

// const ProtectedRoute = ({ isAuthenticated, ...props }: any) =>
//     isAuthenticated ? <Route {...props}/> : <Redirect to="/access"/>;

type Props = ReduxProps<typeof mapState> & RouteComponentProps;

class LayoutContainer extends React.Component<Props> {
    render() {
        return (
            <>
                {!this.props.isLoading && <Logo/>}
                <Switch>
                    <Route exact path="/" component={AccessContainer}/>
                    <Route path="/setup" component={SetupContainer}/> // TODO find how to make it a one-time-only component
                    <Route path={`/${this.props.uid}`} component={ProfileContainer}/>} // TODO change uid to username
                    {!this.props.isLoading && <Route component={NoMatch}/>}
                </Switch>
                {this.props.isLoading && <Loader/>}
            </>
        )
    }
}

export default withRouter(connect(mapState)(LayoutContainer));
