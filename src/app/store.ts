import logger from 'redux-logger';
import { createBrowserHistory } from 'history';
import thunk, { ThunkDispatch } from 'redux-thunk';
import { reducer as formReducer } from 'redux-form';
import { composeWithDevTools } from 'redux-devtools-extension';
import { connectRouter, routerMiddleware } from 'connected-react-router';
import { Store, Action, createStore, applyMiddleware, combineReducers } from 'redux';

import { StateType } from 'src/app/types/state-type';

import app from 'src/app/reducer';
import access from 'src/access/reducer';

const combinedReducers = (history: any) => combineReducers({
    router: connectRouter(history),
    app,
    access,
    form: formReducer,
});

export const history = createBrowserHistory();
const rootReducer = combinedReducers(history);
const middlewares = [routerMiddleware(history), thunk, logger];

export type RootState = StateType<typeof rootReducer>;

type StoreExtensions = { dispatch: ThunkDispatch<RootState, void, Action> };
type ExtendedStore = Store<RootState, Action> & StoreExtensions;

function reduxStore(initialState?: DeepPartial<RootState>): ExtendedStore {
    return createStore(
        rootReducer,
        initialState,
        composeWithDevTools(applyMiddleware(...middlewares))
    );
}

export default reduxStore();
