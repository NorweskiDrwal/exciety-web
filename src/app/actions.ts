import { ActionDefinition } from 'src/app/utils';

export const SET_AUTHENTICATION = ActionDefinition.of('SET_AUTHENTICATION');
