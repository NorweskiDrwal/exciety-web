import { Reducer } from 'redux';

import { AppState } from 'src/app/types';
import * as AppActions from 'src/app/actions';

const initialState: AppState = {
    isAuthenticated: false,
};

const appReducer: Reducer<AppState> = (state = initialState, action): AppState => {
    
    if (AppActions.SET_AUTHENTICATION.is(action)) {
        return { ...state, isAuthenticated: true };
    }
    return state;
};

export default appReducer;
