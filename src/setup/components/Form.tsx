import * as React from 'react';
import styled from 'styled-components';
import { Field, reduxForm, InjectedFormProps } from 'redux-form';

const Styled = {
    Wrapper: styled.div``,
};

type Props = {
    onSubmit(): void,
};

let Form: React.FunctionComponent<InjectedFormProps<{}, {}, string> & Props> = (props: Props) => {
    return (
        <Styled.Wrapper>
            <form onSubmit={props.onSubmit}>
                <div>
                    <label htmlFor="firstName">First Name</label>
                    <Field name="firstName" component="input" type="text" />
                </div>
                <div>
                    <label htmlFor="lastName">Last Name</label>
                    <Field name="lastName" component="input" type="text" />
                </div>
                <div>
                    <label htmlFor="email">Email</label>
                    <Field name="email" component="input" type="email" />
                </div>
                <button type="submit">Submit</button>
            </form>
        </Styled.Wrapper>
    );
};

export default reduxForm<Props>({
    form: 'setup',
})(Form);
