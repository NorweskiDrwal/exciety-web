import * as React from 'react';
import Dropzone from 'react-dropzone';
import styled from 'styled-components';

import { Colors, Centered } from 'src/app/assets';

type CustomStyledProps = {
    isDragReject: boolean,
    isDragAccept: boolean,
    isDragActive: boolean,
    isPictureAdded: boolean | null
}

const Styled = {
    Dropzone: styled('div')<CustomStyledProps>`
        width: 150px;
        height: 150px;
        margin: 0 auto;
        cursor: pointer;
        position: relative;
        border-radius: 50%;
        box-sizing: border-box;
        transition: all .1s linear;
        border-width: ${props => props.isPictureAdded ? '10px' : '3px'};
        border-color: ${props => props.isPictureAdded ? Colors.success : Colors.yellow};
        border-style: ${
            props => props.isDragReject || props.isDragActive
                ? 'solid'
                : props.isPictureAdded
                    ? 'solid'
                    : 'dashed'
        };
        background-color: ${ // TODO switch to green from red on isDragAccept and to '' on isPictureAdded === true -> make a helper function with switch/ifs
            props => props.isDragReject || props.isPictureAdded === false
                ? Colors.error
                : props.isDragAccept || props.isPictureAdded
                    ? Colors.success
                    : ''
        };
        :focus {
            outline: none;
        };
    `,
    Thumbnail: styled.img`
        ${Centered};
        height: 150px;
        overflow: hidden;
    `,
    Text: styled.p`
        ${Centered};
        width: 144px;
        line-height: 144px;
        text-align: center;
    `,
};

type ExtendedFile = File & { preview: string };

type State = {
    isPictureAdded: boolean | null,
    files: ExtendedFile[],
};

class UserPicture extends React.Component {
    state: State = {
        files: [],
        isPictureAdded: null,
    };
    
    onDrop(files: Blob[]) {
        this.setState({
            files: files.map((file) => Object.assign(file, {
                preview: URL.createObjectURL(file)
            }))
        });
    }
    
    onDropAccepted() {
        this.setState({ isPictureAdded: true })
    }
    
    onDropRejected() {
        this.setState({ isPictureAdded: false })
    }
    
    componentWillUnmount() {
        // Make sure to revoke the data uris to avoid memory leaks
        this.state.files.forEach((file: ExtendedFile) => URL.revokeObjectURL(file.preview))
    }
    
    render() {
        const thumbs = this.state.files.map((file: ExtendedFile) => (
            <Styled.Thumbnail
                key={file.name}
                src={file.preview}
            />
        ));
        
        return (
            <Dropzone
                accept="image/*"
                multiple={false}
                onDrop={this.onDrop.bind(this)}
                onDropAccepted={this.onDropAccepted.bind(this)}
                onDropRejected={this.onDropRejected.bind(this)}
            >
                {({ getRootProps, getInputProps, isDragActive, isDragAccept, isDragReject }) => (
                    <Styled.Dropzone
                        isDragActive={isDragActive}
                        isDragReject={isDragReject}
                        isDragAccept={isDragAccept}
                        isPictureAdded={this.state.isPictureAdded}
                        {...getRootProps()}
                    >
                        <input {...getInputProps()} />
                        <Styled.Text>
                            {this.state.isPictureAdded === false
                                ? `${isDragAccept ? 'Drop your picture' : 'Wrong format'}`
                                : this.state.isPictureAdded
                                    ? ''
                                    : isDragReject ? 'Wrong format' : `${isDragAccept ? 'Drop' : 'Drag'} your picture`}
                        </Styled.Text>
                        {thumbs}
                    </Styled.Dropzone>
                )}
            </Dropzone>
        );
    }
}

export default UserPicture;
