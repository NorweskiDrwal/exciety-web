import * as React from 'react';
import styled from 'styled-components';

import { UserPicture } from '.';
import { Form } from 'src/profile/components';
import { Centered, Colors } from 'src/app/assets';

const Styled = {
    Wrapper: styled('div')<{ isLoading: boolean }>`
        ${Centered};
        box-sizing: border-box;
        transition: all .2s ease-out;
        border: 10px solid ${Colors.yellow};
        width: ${props => props.isLoading ? '150px' : '90%'};
        height: ${props => props.isLoading ? '150px' : '80%'};
        border-radius: ${props => props.isLoading ? '6rem' : '3rem'};
        background: ${props => props.isLoading ? 'transparent' : Colors.midgrey};
        @media (min-width: 600px) {
            width: ${props => props.isLoading ? '150px' : '60%'};
        }
    `,
    Panel: styled.div`
        ${Centered};
        width: 90%;
        height: 90%;
    `,
};

type Props = {
    onSubmit(): void,
    isLoading: boolean,
};

const Panel: React.FunctionComponent<Props> = (props) => {
    return (
        <Styled.Wrapper isLoading={props.isLoading}>
            <Styled.Panel>
                {!props.isLoading && <UserPicture/>}
                <Form onSubmit={props.onSubmit}/>
            </Styled.Panel>
        </Styled.Wrapper>
    );
};

export default Panel;
