import * as React from 'react';
import { connect } from 'react-redux';

import { RootState } from 'src/app/store';
import { FetchingStatus, ReduxProps } from 'src/app/utils';

import { Panel } from 'src/setup/components';

const mapState = (state: RootState) => ({
    isAuthenticated: state.app.isAuthenticated,
    uid: state.access.user && state.access.user.uid,
    isLoading: state.access.status === FetchingStatus.LOADING,
});

type Props = ReduxProps<typeof mapState>;
type State = {};

class SetupContainer extends React.Component<Props, State> {
    submitSetup = () => {
        console.log('submit');
    };

    render() {
        return (
            <Panel
                onSubmit={this.submitSetup}
                isLoading={this.props.isLoading}
            />
        )
    }
}

export default connect(mapState)(SetupContainer);
