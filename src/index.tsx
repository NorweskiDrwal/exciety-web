import * as React from 'react';
import ReactDOM from 'react-dom';
import { Provider as StoreProvider } from 'react-redux';

import 'normalize.css';

import store from 'src/app/store';
import { App } from 'src/app/components';

// @ts-ignore
import * as serviceWorker from 'src/serviceWorker';

ReactDOM.render(
    <StoreProvider store={store}>
        <App/>
    </StoreProvider>,
    document.getElementById('root') as HTMLElement
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
