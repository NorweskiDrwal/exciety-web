type Dictionary<T = string> = { readonly [key: string]: T };

type Optional<T> = T | undefined;

type DeepPartial<T> = { [P in keyof T]?: DeepPartial<T[P]> };

type Creator<T> = (...args: any[]) => T;

declare module 'connected-react-router';
