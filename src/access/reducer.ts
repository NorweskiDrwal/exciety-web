import { Reducer } from 'redux';

import { FetchingStatus } from 'src/app/utils';

import { AccessState } from 'src/access/types';
import * as AccessActions from 'src/access/actions';

const initialState: AccessState = {
    status: FetchingStatus.PENDING,
    user: undefined,
};

const accessReducer: Reducer<AccessState> = (state = initialState, action): AccessState => {
    if (AccessActions.OPERATION_START.is(action)) {
        return { ...state, status: FetchingStatus.LOADING };
    }
    if (AccessActions.OPERATION_END.is(action)) {
        return { ...state, status: FetchingStatus.PENDING };
    }
    if (AccessActions.OPERATION_FAIL.is(action)) {
        return { ...state, status: FetchingStatus.FAILURE };
    }
    if (AccessActions.FETCH_USER_SUCCESS.is(action)) {
        return { ...state, status: FetchingStatus.SUCCESS, user: action.payload };
    }
    return state;
};

export default accessReducer;
