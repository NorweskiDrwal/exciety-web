import { ActionDefinition } from 'src/app/utils';

export const OPERATION_START = ActionDefinition.of('OPERATION_START');
export const OPERATION_END = ActionDefinition.of('OPERATION_END');
export const OPERATION_FAIL = ActionDefinition.of('OPERATION_FAIL');

export const FETCH_USER_SUCCESS = ActionDefinition.of('FETCH_USER_SUCCESS');
