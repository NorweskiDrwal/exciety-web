import * as React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';

import { RootState } from 'src/app/store';
import { Colors, Spacing, Centered } from 'src/app/assets';
import { FetchingStatus, ReduxProps } from 'src/app/utils';

import { AccessPoint } from 'src/access/utils';
import * as AccessActions from 'src/access/actions';
import * as AccessAsyncActions from 'src/access/async-actions';

import Form from './Form';

const Styled = {
    Wrapper: styled('div')<{ isLoading: boolean, register: boolean }>`
        ${Centered};
        z-index: 10000;
        box-sizing: border-box;
        padding: ${Spacing.XXL};
        transition: all .2s ease-out;
        border: 10px solid ${Colors.yellow};
        border-radius: ${props => props.isLoading ? '6em' : '3em'};
        width: ${props => props.isLoading ? '150px' : '90%'};
        height: ${props => props.isLoading ? '150px' : '480px'};
        background: ${
            props => props.isLoading ? 'transparent' : props.register ? Colors.purple : Colors.red
        };
        @media (min-width: 600px) {
            width: ${props => props.isLoading ? '150px' : '450px'};
        }
    `,
};

const mapState = (state: RootState) => ({
    isLoading: state.access.status === FetchingStatus.LOADING,
});

const mapDispatch = {
    setLoading: AccessActions.OPERATION_START.create,
    createUser: AccessAsyncActions.createUser,
    loginUser: AccessAsyncActions.loginUser,
};

type Props = ReduxProps<typeof mapState, typeof mapDispatch>;
type State = {
    email: string,
    password: string,
    accessPoint: string,
};
class AccessContainer extends React.Component<Props, State> {
    state: State = {
        email: '',
        password: '',
        accessPoint: AccessPoint.LOGIN,
    };
    
    setEmail = (e: any) => this.setState({ email: e.target.value }); // TODO find correct type
    setPassword = (e: any) => this.setState({ password: e.target.value }); // TODO find correct type
    switchTo = (accessPoint: string) => this.setState({ accessPoint });
    submitTo = (accessPoint: string) => {
        accessPoint === AccessPoint.REGISTER && this.props.createUser({
            email: this.state.email,
            password: this.state.password,
        });
        accessPoint === AccessPoint.LOGIN && this.props.loginUser({
            email: this.state.email,
            password: this.state.password,
        });
    };
    
    render() {
        const login = this.state.accessPoint === AccessPoint.LOGIN;
        
        return (
            <Styled.Wrapper isLoading={this.props.isLoading} register={!login}>
                {
                    login &&
                        <Form
                            {...this.state}
                            switch={this.switchTo}
                            submit={this.submitTo}
                            setEmail={this.setEmail}
                            setPassword={this.setPassword}
                            isLoading={this.props.isLoading}
                        />
                }
                {
                    !login &&
                        <Form
                            register
                            {...this.state}
                            switch={this.switchTo}
                            submit={this.submitTo}
                            setEmail={this.setEmail}
                            setPassword={this.setPassword}
                            isLoading={this.props.isLoading}
                        />
                }
            </Styled.Wrapper>
        );
    }
}

export default connect(mapState, mapDispatch)(AccessContainer);
