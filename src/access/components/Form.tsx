import * as React from 'react';
import styled from 'styled-components';

import { AccessPoint } from 'src/access/utils';

import { Spacing, Colors } from 'src/app/assets';

const Button = styled.button`
    width: 100%;
    border: none;
    font-size: 1rem;
    cursor: pointer;
    border-radius: 0.5rem;
    margin: ${Spacing.ML} 0;
    background-clip: padding-box;
    padding: 14px ${Spacing.M} 10px;
    font-family: 'Josefin Sans', sans-serif;
    :focus {
        outline: none;
    }
`;

const Styled = {
    FormWrapper: styled.div`
        width: 100%;
        text-align: center;
        font-family: 'Josefin Sans', sans-serif;
    `,
    Title: styled.p`
        font-size: 1.2rem;
        color: ${Colors.yellow};
        margin: 0 auto ${Spacing.ML};
    `,
    Input: styled.input`
        width: calc(100% - 16px);
        border: none;
        font-size: 1rem;
        border-radius: 0.5rem;
        padding: 14px ${Spacing.M} 10px;
        margin: ${Spacing.ML} auto;
        background-clip: padding-box;
        font-family: 'Josefin Sans', sans-serif;
        :focus {
            outline: none;
        }
    `,
    LoginButton: styled(Button)` && {
        color: ${Colors.red};
    }`,
    RegisterButton: styled(Button)` && {
        color: ${Colors.purple};
    }`,
    Divider: styled.hr`
        border: 0;
        border-top: 1px solid rgba(0,0,0,.1);
    `,
    Text: styled.p`
        color: ${Colors.yellow};
        margin: ${Spacing.ML} auto;
    `,
    FacebookButton: styled(Button)` && {
        margin-top: 0;
        color: ${Colors.white};
        background: ${Colors.facebook};
    }`,
    Link: styled.span`
        cursor: pointer;
        text-decoration: underline;
    `,
};

type Props = {
    email: string,
    setEmail(e: any): void, // TODO find correct type
    password: string,
    isLoading: boolean,
    register?: boolean,
    setPassword(e: any): void, // TODO find correct type
    submit(accessPoint: string): void,
    switch(accessPoint: string): void,
}

const Form: React.FunctionComponent<Props> = (props) => {
    const whichSwitch = () => {
        props.register
            ? props.switch(AccessPoint.LOGIN)
            : props.switch(AccessPoint.REGISTER)
    };
    
    const whichSubmit = (e: any) => { // TODO find correct type
        e.preventDefault();
        props.register
            ? props.submit(AccessPoint.REGISTER)
            : props.submit(AccessPoint.LOGIN)
    };
    
    const form = (
        <Styled.FormWrapper>
            <Styled.Title>
                {props.register
                    ? 'Welcome to Exciety!'
                    : 'The world of excieties awaits!'
                }
            </Styled.Title>
            <Styled.Divider/>
            <Styled.Input type="email" placeholder="email" value={props.email} onChange={props.setEmail}/>
            <Styled.Input type="password" placeholder="password" value={props.password} onChange={props.setPassword}/>
            {props.register && <Styled.RegisterButton onClick={whichSubmit}>Register</Styled.RegisterButton>}
            {!props.register && <Styled.LoginButton onClick={whichSubmit}>Login</Styled.LoginButton>}
            <Styled.Divider/>
            <Styled.Text>
                Or sign {props.register ? 'up' : 'in'} with:
            </Styled.Text>
            <Styled.FacebookButton>Facebook</Styled.FacebookButton>
            <Styled.Text style={{ fontSize: '0.9rem'}}>
                {props.register && <span>Back to <Styled.Link onClick={whichSwitch}>login</Styled.Link></span>}
                {!props.register && <span><Styled.Link onClick={whichSwitch}>Click here</Styled.Link> if you don't have an account</span>}
            </Styled.Text>
        </Styled.FormWrapper>
    );
    
    return (
        <>{!props.isLoading && form}</>
    );
};

export default Form;
