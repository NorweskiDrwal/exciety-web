import { Dispatch } from 'redux';
import { push } from 'connected-react-router';
import firebase, {FirebaseError, User} from 'firebase';

import { db, auth } from 'src/firebase';

import * as AppActions from 'src/app/actions';
import { FetchingStatus } from 'src/app/utils';
import * as AccessActions from 'src/access/actions';

type Credentials = {
    email: string,
    password: string,
};

const errorHandler = (error: FirebaseError): any => (dispatch: Dispatch) => {
    dispatch(AccessActions.OPERATION_FAIL.create());
    console.log(error.message);
};

export const loginUser = (credentials: Credentials): any => // TODO find correct type
    (dispatch: Dispatch, getState: any) => {
        getState().access.status !== FetchingStatus.LOADING && dispatch(AccessActions.OPERATION_START.create());
        
        auth.signInWithEmailAndPassword(credentials.email, credentials.password)
            .then(() => dispatch(fetchUser()))
            .catch((error: FirebaseError) => dispatch(errorHandler(error)));
    };

let isCreate: boolean;
export const createUser = (credentials: Credentials): any => // TODO find correct type
    (dispatch: Dispatch) => {
        dispatch(AccessActions.OPERATION_START.create());
        
        isCreate = true;
        auth.createUserWithEmailAndPassword(credentials.email, credentials.password)
            .then((user: firebase.auth.UserCredential) => {
                user && dispatch(push("setup"));
                if (user) {
                    const uid = user.user && user.user.uid;
                    db.collection('users').doc(uid).set({ uid, email: credentials.email })
                        .then(() => dispatch(AccessActions.OPERATION_END.create()))
                        .catch((error: FirebaseError) => dispatch(errorHandler(error)));
                }
            })
            .catch((error: FirebaseError) => dispatch(errorHandler(error)));
    };

export const fetchUser = (): any => // TODO find correct type
    (dispatch: Dispatch, getState: any) => {
        dispatch(AccessActions.OPERATION_START.create());
        
        auth.onAuthStateChanged((user: User) => {
            if (user && !isCreate) {
                const profileLocation = getState().router.location.pathname === `/${user.uid}`;
                !profileLocation && dispatch(push(`/${user.uid}`));
                
                db.collection('users').doc(user.uid).get()
                    .then((doc: any) => {
                        const userFetched = getState().access.user;
                        if (doc.exists && !userFetched) {
                            dispatch(AppActions.SET_AUTHENTICATION.create());
                            dispatch(AccessActions.FETCH_USER_SUCCESS.create(doc.data()));
                        }
                    })
                    .then(() => isCreate = false)
                    .catch((error: FirebaseError) => dispatch(errorHandler(error)));
            } else !isCreate && dispatch(AccessActions.OPERATION_END.create());
        })
    };

export const logoutUser = (): any => (dispatch: Dispatch) => {
    auth.signOut()
        .then(() => {
            window.location.reload();
            window.location.replace("/")
        })
};
