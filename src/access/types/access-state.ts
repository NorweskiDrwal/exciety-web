import { FetchingStatus } from 'src/app/utils';

export interface AccessState {
    readonly status: FetchingStatus;
    readonly user: any; // TODO set correct user type
}
