import * as firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';


// used for prod build (gatsby build)
    // const prodConfig = {
    //   apiKey: "AIzaSyA5bCnOpcmhtCCDzSA8E0a4sxQ5YzQGGDg",
    //   authDomain: "flashpub-67105.firebaseapp.com",
    //   databaseURL: "https://flashpub-67105.firebaseio.com",
    //   projectId: "flashpub-67105",
    //   storageBucket: "flashpub-67105.appspot.com",
    //   messagingSenderId: "581239607146",
// };

// used when running locally (gatsby develop)
const devConfig = {
    apiKey: "AIzaSyABrBHeGvgPkhjJ7ak_kZpwbBOMvbeRw9U",
    authDomain: "exciety-dev.firebaseapp.com",
    databaseURL: "https://exciety-dev.firebaseio.com",
    projectId: "exciety-dev",
    storageBucket: "exciety-dev.appspot.com",
    messagingSenderId: "27445231952"
};

const config = process.env.NODE_ENV === 'production'
    ? devConfig // FOR PROD RELEASES: CHANGE THIS TO 'prodConfig'
    : devConfig;

let db: any;
let auth: any;
let storage: any;

if (!firebase.apps.length) {
    firebase.initializeApp(config);
}

if (typeof window !== "undefined") {
    db = firebase.firestore();
    db.settings({timestampsInSnapshots: true});
    auth = firebase.auth();
    // storage = firebase.storage();
}

export {
    db,
    auth,
    storage
};
