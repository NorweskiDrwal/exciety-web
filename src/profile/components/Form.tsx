import * as React from 'react';
import styled from 'styled-components';

import { Centered, Colors } from 'src/app/assets';

const Styled = {
    Wrapper: styled('div')<{ isLoading: boolean }>`
        ${Centered};
        border-radius: ${props => props.isLoading ? '6rem' : '3rem'};
        box-sizing: border-box;
        transition: all .2s ease-out;
        border: 10px solid ${Colors.yellow};
        width: ${props => props.isLoading ? '150px' : '90%'};
        height: ${props => props.isLoading ? '150px' : '80%'};
        background: ${props => props.isLoading ? 'transparent' : Colors.midgrey};
        @media (min-width: 600px) {
            width: ${props => props.isLoading ? '150px' : '60%'};
        }
    `,
};

type Props = {
    isLoading: boolean,
};
const Form: React.FunctionComponent<Props> = (props) => {
    return (
        <Styled.Wrapper isLoading={props.isLoading}>
        
        </Styled.Wrapper>
    );
};

export default Form;
