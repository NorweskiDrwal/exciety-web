import * as React from 'react';
import { connect } from 'react-redux';

import { RootState } from 'src/app/store';
import { FetchingStatus, ReduxProps } from 'src/app/utils';

import * as AccessAsyncActions from 'src/access/async-actions';
import { Form } from 'src/profile/components';

const mapState = (state: RootState) => ({
    isAuthenticated: state.app.isAuthenticated,
    uid: state.access.user && state.access.user.uid,
    isLoading: state.access.status === FetchingStatus.LOADING,
});
const mapDispatch = {
    logout: AccessAsyncActions.logoutUser,
};
type Props = ReduxProps<typeof mapState, typeof mapDispatch>;
type State = {}

class ProfileContainer extends React.Component<Props, State> {
    render() {
        return (
            <>
                {console.log(this.props.isLoading)}
                <Form isLoading={this.props.isLoading}/>
                <p onClick={this.props.logout}>Profile content</p>
            </>
        )
    }
}

export default connect(mapState, mapDispatch)(ProfileContainer);
